-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema db_AparClic
--
-- Base de datos para la aplicacion AparClic
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `db_AparClic` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `db_AparClic` ;

-- -----------------------------------------------------
-- Table `db_AparClic`.`tb_parqueaderos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_AparClic`.`tb_parqueaderos` (
  `id_parqueadero` INT NOT NULL AUTO_INCREMENT,
  `direccion_parqueadero` VARCHAR(45) NOT NULL,
  `tarifa_vehiculo` INT NOT NULL,
  `tarifa_moto` INT NOT NULL,
  `cupos_vehiculos` INT NOT NULL,
  `cupos_motos` INT NOT NULL,
  `cupos_exclusivos` INT NOT NULL,
  `hora_apertura` TIME NOT NULL,
  `hora_cierre` TIME NOT NULL,
  PRIMARY KEY (`id_parqueadero`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `db_AparClic`.`tb_usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_AparClic`.`tb_usuarios` (
  `id_usuario` INT NOT NULL,
  `nombre_usuario` VARCHAR(45) NOT NULL,
  `usuario` VARCHAR(45) NOT NULL,
  `contrasena` VARCHAR(45) NOT NULL,
  `estado_usuario` TINYINT NOT NULL,
  `fecha_Activo` DATE NOT NULL,
  `fecha_inactivo` DATE NOT NULL,
  `administrador` TINYINT NOT NULL,
  PRIMARY KEY (`id_usuario`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `db_AparClic`.`tb_liquida_tarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `db_AparClic`.`tb_liquida_tarifa` (
  `id_liquidacion` INT NOT NULL AUTO_INCREMENT,
  `id_parqueadero` INT NOT NULL,
  `id_usuario` INT NOT NULL,
  `num_cupo` INT NOT NULL,
  `placa_automotor` VARCHAR(10) NOT NULL,
  `tipo_automotor` INT NOT NULL,
  `fecha_hora_ingreso` DATETIME NOT NULL,
  `fecha_hora_salida` DATETIME NULL,
  `valor_liquidado` INT NULL,
  `base_iva` INT NULL,
  `num_factura` VARCHAR(45) NULL,
  `estado_liquidacion` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_liquidacion`),
  CONSTRAINT `fk_id_liquida_tarifa_parqueadero`
    FOREIGN KEY (`id_parqueadero`)
    REFERENCES `db_AparClic`.`tb_parqueaderos` (`id_parqueadero`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_id_liquida_tarifa_usuario`
    FOREIGN KEY (`id_usuario`)
    REFERENCES `db_AparClic`.`tb_usuarios` (`id_usuario`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE INDEX `fk_id_liquida_tarifa_usuario_idx` ON `db_AparClic`.`tb_liquida_tarifa` (`id_usuario` ASC) VISIBLE;

CREATE INDEX `fk_id_liquida_tarifa_parqueadero_idx` ON `db_AparClic`.`tb_liquida_tarifa` (`id_parqueadero` ASC) VISIBLE;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

/*Inserción tabla tb_usuarios*/
insert into db_AparClic.tb_usuarios (id_usuario, nombre_usuario, usuario, contrasena, estado_usuario, fecha_Activo, fecha_inactivo, administrador)
values (80000000, 'NESTOR MIGUEL RICO RICO', 'nrico23', sha1('Contrasena1'), 1, '2021-09-04' , '2021-12-31', 1);
/*Consultar tabla tb_usuarios*/
SELECT * FROM db_AparClic.tb_usuarios;

/*Inserción tabla tb_parqueaderos*/
insert into db_AparClic.tb_parqueaderos (id_parqueadero, direccion_parqueadero, tarifa_vehiculo, tarifa_moto, cupos_vehiculos ,cupos_motos, cupos_exclusivos, hora_apertura, hora_cierre)
values (0,'CL 23 05 73', 100, 50, 20, 40, 5, '06:00:00','22:00:00') ,(0,'CL 19 07 48', 150, 100, 40, 30, 5, '07:00:00','21:00:00');
/*Consultar tabla tb_parqueaderos*/
SELECT * FROM db_AparClic.tb_parqueaderos;

/*Inserción tabla tb_liquida_tarifa*/
insert into db_AparClic.tb_liquida_tarifa (id_liquidacion, id_parqueadero, id_usuario, num_cupo, placa_automotor, tipo_automotor, fecha_hora_ingreso, fecha_hora_salida, valor_liquidado, base_iva, num_factura)
values (0, 1, 80000000, 1, 'LLY28C', 2, '2021-09-06 06:05:10', NULL, NULL, NULL,NULL);
/*Consultar tabla tb_liquida_tarifa*/
SELECT * FROM db_AparClic.tb_liquida_tarifa;
