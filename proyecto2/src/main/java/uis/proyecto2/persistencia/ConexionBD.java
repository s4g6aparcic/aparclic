package uis.proyecto2.persistencia;
/**
 *
 * @author MariaFernanda
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ConexionBD {
    Connection con;
// Configuracion de la conexion a la base de datos
    private String DB_driver = "";
    private String url = "";
    private String db = "";
    private String host = "";
    private String username = "";
    private String password = "";
    private Statement stmt = null;
    //private PreparedStatement pstmt  = null;
    private ResultSet rs = null;
    private boolean local;
    //Constructor sin parametros
    public ConexionBD() {
        local = true;//puede establecer este valor en falso para conectarse al servidor remoto
        DB_driver = "com.mysql.cj.jdbc.Driver";
        if (local) {
            host = "localhost:3306";
            db = "db_aparclic";
            url = "jdbc:mysql://" + host + "/" + db; //URL DB
            username = "root"; //usuario base de datos global
            password = "12345678";
        } else {
            host = "mysql1007.mochahost.com:3306";
            db = "db_aparclic";
            url = "jdbc:mysql://" + host + "/" + db; //URL DB
            username = "root"; //usuario base de datos global
            password = "12345678";
        }
        try {
            //Asignacion del Driver
            Class.forName(DB_driver);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            // Realizar la conexion
            con = DriverManager.getConnection(url, username, password);
            con.setTransactionIsolation(8);
            System.out.println("conectado");
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Realizar la conexion
    }
    //Retornar la conexion
    public Connection getConnection() {
        return con;
    }
    //Cerrar la conexion
    public void closeConnection(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    // Metodo que devuelve un ResultSet de una consulta (tratamiento de SELECT)
    public ResultSet consultarBD(String sentencia) {
        try {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rs = stmt.executeQuery(sentencia);
        } catch (SQLException sqlex) {
        } catch (RuntimeException rex) {
        } catch (Exception ex) {
        }
        return rs;
    }
    // Metodo que realiza un INSERT y devuelve TRUE si la operacion fue existosa
    public boolean insertarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    public boolean borrarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.execute(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    // Metodoque realiza una operacion como UPDATE, DELETE, CREATE TABLE, entre otras
    // y devuelve TRUE si la operacion fue existosa
    public boolean actualizarBD(String sentencia) {
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(sentencia);
        } catch (SQLException | RuntimeException sqlex) {
            System.out.println("ERROR RUTINA: " + sqlex);
            return false;
        }
        return true;
    }
    public boolean setAutoCommitBD(boolean parametro) {
        try {
            con.setAutoCommit(parametro);
        } catch (SQLException sqlex) {
            System.out.println("Error al configurar el autoCommit" + sqlex.getMessage());
            return false;
        }
        return true;
    }
    public void cerrarConexion() {
        closeConnection(con);
    }
    public boolean commitBD() {
        try {
            con.commit();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer commit " + sqlex.getMessage());
            return false;
        }
    }
    public boolean rollbackBD() {
        try {
            con.rollback();
            return true;
        } catch (SQLException sqlex) {
            System.out.println("Error al hacer rollback " + sqlex.getMessage());
            return false;
        }
    }
    public static void main(String[] args) {
        ConexionBD b = new ConexionBD();
        //crear un arraylist de tipo usuario
        ResultSet rst=b.consultarBD("SELECT * FROM db_AparClic.tb_usuarios;");
        try {
            while (rst.next()) {
                System.out.println(rst.getString("nombre_usuario"));
                //crear un objeto de tipo usuario y agregarlo al vector y
                //asignarle los valores de la bd
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
        }
        b.cerrarConexion();
    }
}