/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uis.proyecto2.logica;
/**
 *
 * @author MariaFernanda
 */
public class Usuario {
    private int id_usuario;
    private String nombre_usuario;
    private String usuario;
    private String contrasena;
    private int estado_usuario;
    private String fecha_activo;
    private String fecha_inactivo;
    private int administrador;

    public Usuario() {
    }

    public Usuario(String nombre_usuario, String usuario, String contrasena, int estado_usuario, String fecha_activo, String fecha_inactivo, int administrador) {
        this.nombre_usuario = nombre_usuario;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.estado_usuario = estado_usuario;
        this.fecha_activo = fecha_activo;
        this.fecha_inactivo = fecha_inactivo;
        this.administrador = administrador;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getNombre_usuario() {
        return nombre_usuario;
    }

    public void setNombre_usuario(String nombre_usuario) {
        this.nombre_usuario = nombre_usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getEstado_usuario() {
        return estado_usuario;
    }

    public void setEstado_usuario(int estado_usuario) {
        this.estado_usuario = estado_usuario;
    }

    public String getFecha_activo() {
        return fecha_activo;
    }

    public void setFecha_activo(String fecha_activo) {
        this.fecha_activo = fecha_activo;
    }

    public String getFecha_inactivo() {
        return fecha_inactivo;
    }

    public void setFecha_inactivo(String fecha_inactivo) {
        this.fecha_inactivo = fecha_inactivo;
    }

    public int getAdministrador() {
        return administrador;
    }

    public void setAdministrador(int administrador) {
        this.administrador = administrador;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id_usuario=" + id_usuario + ", nombre_usuario=" + nombre_usuario + ", usuario=" + usuario + ", contrasena=" + contrasena + ", estado_usuario=" + estado_usuario + ", fecha_activo=" + fecha_activo + ", fecha_inactivo=" + fecha_inactivo + ", administrador=" + administrador + '}';
    }

    
}
