<%-- 
    Document   : index
    Created on : 25/09/2021, 08:08:14 PM
    Author     : MariaFernanda
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Connection con;
            String url="jdbc:mysql://localhost:3306/db_AparClic";
            String Driver="com.mysql.cj.jdbc.Driver";
            String user="root";
            String clave="12345678";
            Class.forName(Driver);
            con=DriverManager.getConnection(url,user,clave);
            PreparedStatement ps;
            ResultSet rs;
            ps=con.prepareStatement("SELECT * FROM db_AparClic.tb_usuarios;");
            rs=ps.executeQuery();
        %>
        <div class="container">
            <h1>Lista de Usuarios</h1>
            <hr>
            <a class="btn btn-success btn-lg" href="crear.jsp">Nuevo Registro</a>
            <br>
            <br>
            <table class="table table-bordered">
                <tr>
                    <th class="text-center">ID Usuario</th>
                    <th class="text-center">Nombre Usuario</th>
                    <th class="text-center">Usuario</th>
                    <th class="text-center">Contraseña</th>
                    <th class="text-center">Estado_usuario</th>
                    <th class="text-center">Fecha Activo</th>
                    <th class="text-center">Fecha Inactivo</th>
                    <th class="text-center">Administrador</th>
                    <th class="text-center">ACCIONES</th>
                </tr>
                <%
                    while(rs.next()) {    
                %>
                <tr>
                    <td class="text-center"><%= rs.getInt("id_usuario")%></td>
                    <td class="text-center"><%= rs.getString("nombre_usuario")%></td>
                    <td class="text-center"><%= rs.getString("usuario")%></td>
                    <td class="text-center">*****</td>
                    <td class="text-right"><%= rs.getInt("estado_usuario")%></td>
                    <td class="text-center"><%= rs.getString("fecha_activo")%></td>
                    <td class="text-center"><%= rs.getString("fecha_inactivo")%></td>
                    <td class="text-center"><%= rs.getInt("administrador")%></td>
                    <td class="text-center">
                        <a href="update.jsp?id=<%= rs.getInt("id_usuario")%>" class="btn btn-warning btn-sm">Editar</a>
                        <a href="delete.jsp?id=<%= rs.getInt("id_usuario")%>" class="btn btn-danger btn-sm">Eliminar</a>
                    </td>
                </tr>
                <%}%>
            </table>
        </div>
    </body>
</html>
