<%-- 
    Document   : delete
    Created on : 27/09/2021, 02:54:33 PM
    Author     : MariaFernanda
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Connection con;
            String url="jdbc:mysql://localhost:3306/db_AparClic";
            String Driver="com.mysql.cj.jdbc.Driver";
            String user="root";
            String clave="12345678";
            Class.forName(Driver);
            con=DriverManager.getConnection(url,user,clave);
            PreparedStatement ps;
            int id=Integer.parseInt(request.getParameter("id")); //capturamos en index al darle el boton Eliminar
            ps=con.prepareStatement("DELETE FROM db_AparClic.tb_usuarios where id_usuario="+id);
            ps.executeUpdate();
            response.sendRedirect("index.jsp");
        %>
    </body>
</html>
