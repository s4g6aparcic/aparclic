<%-- 
    Document   : update
    Created on : 27/09/2021, 02:53:59 PM
    Author     : MariaFernanda
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <%
            Connection con;
            String url="jdbc:mysql://localhost:3306/db_AparClic";
            String Driver="com.mysql.cj.jdbc.Driver";
            String user="root";
            String clave="12345678";
            Class.forName(Driver);
            con=DriverManager.getConnection(url,user,clave);
            PreparedStatement ps;
            ResultSet rs;
            int id=Integer.parseInt(request.getParameter("id")); //capturamos en index al darle el boton Editar
            ps=con.prepareStatement("SELECT * FROM db_AparClic.tb_usuarios where id_usuario="+id);
            rs=ps.executeQuery();
            while(rs.next()) {
        %>
        <div class="container">
            <h1>Actualizar Registro</h1>
            <hr>
            <form action="" method="post" class="form-control" style="width:500px; height:450px">
                ID Usuario:
                <input type="number" readonly="" class="form-control" value="<%= rs.getInt("id_usuario")%>"/>
                Nombre Usuario:
                <input type="text" name="txtNom" class="form-control" value="<%= rs.getString("nombre_usuario")%>"/>
                Usuario:
                <input type="text" name="txtUsu" class="form-control" value="<%= rs.getString("usuario")%>"/>
                Contraseña:
                <input type="password" name="txtCon" class="form-control" value="<%= rs.getString("contrasena")%>"/>
                Estado Usuario:
                <input type="number" name="txtEst" class="form-control" value="<%= rs.getInt("estado_usuario")%>"/>
                Fecha Activo (aaaa-mm-dd):
                <input type="date" name="txtFA" class="form-control" value="<%= rs.getString("fecha_activo")%>"/>
                Fecha Inactivo (aaaa-mm-dd):
                <input type="date" name="txtFI" class="form-control" value="<%= rs.getString("fecha_inactivo")%>"/>
                Administrador:
                <input type="number" name="txtAdm" class="form-control" value="<%= rs.getInt("administrador")%>"/>
                <br>
                <br>
                <input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
                <a href="index.jsp">Regresar</a>
            </form>
            <%}%>
        </div>
    </body>
</html>
<%
    String nom1, usu1, cont1, est1, fa1, fi1, adm1;
    nom1 = request.getParameter("txtNom");
    usu1 = request.getParameter("txtUsu");
    cont1 = request.getParameter("txtCon");
    est1 = request.getParameter("txtEst");
    fa1 = request.getParameter("txtFA");
    fi1 = request.getParameter("txtFI");
    adm1 = request.getParameter("txtAdm");
    if (nom1 != null && usu1 != null && cont1 != null && est1 != null && fa1 != null && fi1 != null && adm1 != null) {
        ps = con.prepareStatement("UPDATE db_AparClic.tb_usuarios SET nombre_usuario='" + nom1 + "',usuario='" + usu1 + "',contrasena='" + cont1 + "',estado_usuario='" + est1 + "',fecha_Activo='" + fa1 + "',fecha_inactivo='" + fi1 + "',administrador='" + adm1 + "'WHERE id_usuario="+id);
        System.out.println(ps);
        ps.executeUpdate();
        response.sendRedirect("index.jsp");
    }
%>