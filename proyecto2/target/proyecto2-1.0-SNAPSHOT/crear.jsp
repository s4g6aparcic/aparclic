<%-- 
    Document   : crear
    Created on : 27/09/2021, 01:15:26 PM
    Author     : MariaFernanda
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <title>JSP Page</title>
    </head>
    <body>
        <div class="container">
            <h1>Crear Registro</h1>
            <hr>
            <form action="" method="post" class="form-control" style="width:500px; height:450px">
                ID Usuario:
                <input type="number" name="txtId" class="form-control"/>
                Nombre Usuario:
                <input type="text" name="txtNom" class="form-control"/>
                Usuario:
                <input type="text" name="txtUsu" class="form-control"/>
                Contraseña:
                <input type="password" name="txtCon" class="form-control"/>
                Estado Usuario:
                <input type="number" name="txtEst" class="form-control"/>
                Fecha Activo (aaaa-mm-dd):
                <input type="date" name="txtFA" class="form-control"/>
                Fecha Inactivo (aaaa-mm-dd):
                <input type="date" name="txtFI" class="form-control"/>
                Administrador:
                <input type="number" name="txtAdm" class="form-control"/>
                <br>
                <br>
                <input type="submit" value="Guardar" class="btn btn-primary btn-lg"/>
                <a href="index.jsp">Regresar</a>
            </form>
        </div>
    </body>
</html>
<%
            Connection con;
            String url="jdbc:mysql://localhost:3306/db_AparClic";
            String Driver="com.mysql.cj.jdbc.Driver";
            String user="root";
            String clave="12345678";
            Class.forName(Driver);
            con=DriverManager.getConnection(url,user,clave);
            PreparedStatement ps;
            String id, nom, usu, cont, est, fa, fi, adm;
            id=request.getParameter("txtId");
            nom=request.getParameter("txtNom");
            usu=request.getParameter("txtUsu");
            cont=request.getParameter("txtCon");
            est=request.getParameter("txtEst");
            fa=request.getParameter("txtFA");
            fi=request.getParameter("txtFI");
            adm=request.getParameter("txtAdm");
            if(id!=null && nom!=null && usu!=null && cont!=null && est!=null && fa!=null && fi!=null && adm!=null) {
                ps=con.prepareStatement("insert into db_AparClic.tb_usuarios(id_usuario,nombre_usuario,usuario,contrasena,estado_usuario,fecha_Activo,fecha_inactivo,administrador) values('"+id+"','"+nom+"','"+usu+"','"+cont+"','"+est+"','"+fa+"','"+fi+"','"+adm+"')");
                ps.executeUpdate();
                response.sendRedirect("index.jsp");
            }
%>